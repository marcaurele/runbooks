teams:
- name: sre_reliability
  url: https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/#core-infra
  slack_channel: production
  engagement_policy:
  oncall_schedule: https://gitlab.pagerduty.com/schedules#P22HQSG

- name: create
  url: https://about.gitlab.com/handbook/engineering/development/dev/create/
  manager_slug: dsatcher
  slack_channel: g_create
  engagement_policy:  # Link to doc that talks about engagement and escalation policy
  oncall_schedule:    # Link to pagerduty oncall schedule

- name: distribution_deploy
  url: https://about.gitlab.com/handbook/engineering/dev-backend/distribution/
  slack_channel: g_distribution
  engagement_policy:  # Link to doc that talks about engagement and escalation policy
  oncall_schedule:    # Link to pagerduty oncall schedule
  product_stage_group: distribution_deploy
  ignored_components:
  - graphql_query
- name: geo
  url: https://about.gitlab.com/handbook/engineering/dev-backend/geo/
  manager_slug: geo-mgr
  slack_channel: g_geo
  engagement_policy:  # Link to doc that talks about engagement and escalation policy
  oncall_schedule:    # Link to pagerduty oncall schedule
  product_stage_group: geo
  ignored_components:
  - graphql_query

- name: gitaly_cluster
  url: https://about.gitlab.com/handbook/engineering/dev-backend/gitaly/
  slack_alerts_channel: gitaly-alerts
  product_stage_group: gitaly_cluster
  send_slo_alerts_to_team_slack_channel: true
  ignored_components:
  - graphql_query

- name: gitaly_git
  url: https://about.gitlab.com/handbook/engineering/dev-backend/gitaly/
  slack_alerts_channel: gitaly-alerts
  product_stage_group: gitaly_git
  ignored_components:
  - graphql_query

- name: manage
  url: https://about.gitlab.com/handbook/engineering/dev-backend/manage/
  manager_slug: ruben-d
  slack_channel: g_manage
  engagement_policy:  # Link to doc that talks about engagement and escalation policy
  oncall_schedule:    # Link to pagerduty oncall schedule

- name: plan
  url: https://about.gitlab.com/handbook/engineering/dev-backend/manage/
  manager_slug: ean-m
  slack_channel: s_plan
  engagement_policy:  # Link to doc that talks about engagement and escalation policy
  oncall_schedule:    # Link to pagerduty oncall schedule

- name: release
  url: https://about.gitlab.com/handbook/engineering/dev-backend/
  manager_slug: darby-frey
  slack_channel: g_release
  engagement_policy:  # Link to doc that talks about engagement and escalation policy
  oncall_schedule:    # Link to pagerduty oncall schedule
  product_stage_group: release
  send_error_budget_weekly_to_slack: true
  slack_error_budget_channel: ops-section
  ignored_components:
  - graphql_query

- name: release-management
  url: https://about.gitlab.com/handbook/engineering/development/ci-cd/release/release-management/
  manager_slug: sean_carrol
  slack_channel: g_release_management
  engagement_policy:  # Link to doc that talks about engagement and escalation policy
  oncall_schedule:    # Link to pagerduty oncall schedule

- name: support
  url: https://about.gitlab.com/handbook/support/
  slack_channel: support_gitlab-com
  engagement_policy:  # Link to doc that talks about engagement and escalation policy
  oncall_schedule: https://gitlab.pagerduty.com/schedules#PIQ317K

- name: container_registry
  url: https://about.gitlab.com/handbook/engineering/development/ops/package/container-registry/
  slack_channel: g_container-registry
  slack_alerts_channel: g_container-registry_alerts
  send_slo_alerts_to_team_slack_channel: true
  product_stage_group: container_registry
  send_error_budget_weekly_to_slack: true
  slack_error_budget_channel: ops-section
  ignored_components:
  - graphql_query

- name: package_registry
  url: https://about.gitlab.com/handbook/engineering/development/ops/package/package-registry/
  slack_channel: g_package-registry
  slack_alerts_channel: g_package-registry_alerts
  send_slo_alerts_to_team_slack_channel: true
  product_stage_group: package_registry
  send_error_budget_weekly_to_slack: true
  slack_error_budget_channel: ops-section
  ignored_components:
  - graphql_query

- name: runner
  product_stage_group: runner
  slack_alerts_channel: alerts-ci-cd
  send_error_budget_weekly_to_slack: true
  slack_error_budget_channel: ops-section
  ignored_components:
  - graphql_query

# From https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/344
- name: gitlab-pages
  slack_alerts_channel: gitlab-pages

- name: data-analytics
  url: https://about.gitlab.com/handbook/business-ops/data-team/platform/
  slack_channel: data-engineering
  slack_alerts_channel: data-prom-alerts
  oncall_schedule: https://about.gitlab.com/handbook/business-ops/data-team/how-we-work/duties/

- name: delivery
  slack_alerts_channel: g_delivery
  product_stage_group: delivery
  ignored_components:
  - graphql_query

- name: scalability
  slack_alerts_channel: g_scalability
  product_stage_group: scalability
  ignored_components:
  - graphql_query

- name: scalability-619-redis-k8s
  slack_alerts_channel: scalability-619-redis-k8s
  send_slo_alerts_to_team_slack_channel: true

- name: scalability-823-introduce-redis-cluster
  slack_alerts_channel: scalability-823-introduce-redis-cluster
  send_slo_alerts_to_team_slack_channel: true

- name: scalability-857-redis-functional-partitioning
  slack_alerts_channel: scalability-857-redis-functional-partitioning
  send_slo_alerts_to_team_slack_channel: true

# Workhorse is a virtual team
- name: workhorse
  slack_alerts_channel: workhorse

# Rapid Action Intercom
- name: rapid-action-intercom
  slack_alerts_channel: rapid-action-intercom

- name: pipeline_validation
  slack_alerts_channel: f_pipeline_validation_service
  send_slo_alerts_to_team_slack_channel: true

- name: anti_abuse
  slack_alerts_channel: feed_pipeline_abuse_alerts
  send_slo_alerts_to_team_slack_channel: true

- name: subtransaction_troubleshooting
  slack_alerts_channel: subtransaction_troubleshooting
  send_slo_alerts_to_team_slack_channel: true

- name: configure
  product_stage_group: configure
  slack_alerts_channel: feed_alerts_configure
  send_slo_alerts_to_team_slack_channel: true
  send_error_budget_weekly_to_slack: true
  slack_error_budget_channel: ops-section
  ignored_components:
  - graphql_query

- name: authentication_and_authorization
  slack_alerts_channel: feed_alerts_access
  product_stage_group: authentication_and_authorization
  send_slo_alerts_to_team_slack_channel: true
  ignored_components:
  - graphql_query

- name: global_search
  slack_alerts_channel: g_global_search
  product_stage_group: global_search
  ignored_components:
  - graphql_query

- name: 5-min-app
  product_stage_group: 5-min-app
  ignored_components:
  - graphql_query
- name: activation
  product_stage_group: activation
  ignored_components:
  - graphql_query
- name: ai_assisted
  product_stage_group: ai_assisted
  ignored_components:
  - graphql_query
- name: certify
  url: https://about.gitlab.com/handbook/engineering/development/dev/plan-product-planning-certify-be/
  product_stage_group: certify
  slack_channel: s_plan-be
  slack_alerts_channel: s_plan-be
  ignored_components:
  - graphql_query
- name: code_review
  product_stage_group: code_review
  ignored_components:
  - graphql_query
- name: compliance
  product_stage_group: compliance
  ignored_components:
  - graphql_query
- name: composition_analysis
  product_stage_group: composition_analysis
  ignored_components:
  - graphql_query
- name: acquisition
  product_stage_group: acquisition
  ignored_components:
  - graphql_query
- name: database
  product_stage_group: database
  ignored_components:
  - graphql_query
- name: dataops
  product_stage_group: dataops
  ignored_components:
  - graphql_query
- name: dynamic_analysis
  product_stage_group: dynamic_analysis
  ignored_components:
  - graphql_query
- name: editor
  product_stage_group: editor
  ignored_components:
  - graphql_query
- name: foundations
  product_stage_group: foundations
  ignored_components:
  - graphql_query
- name: dedicated
  product_stage_group: dedicated
  ignored_components:
  - graphql_query
- name: import
  product_stage_group: import
  ignored_components:
  - graphql_query
- name: integrations
  product_stage_group: integrations
  ignored_components:
  - graphql_query
- name: provision
  product_stage_group: provision
  ignored_components:
  - graphql_query
- name: application_performance
  product_stage_group: application_performance
  ignored_components:
  - graphql_query
- name: mlops
  product_stage_group: mlops
  ignored_components:
  - graphql_query
- name: mobile_devops
  product_stage_group: mobile_devops
  ignored_components:
  - graphql_query
- name: respond
  product_stage_group: respond
  send_error_budget_weekly_to_slack: true
  slack_error_budget_channel: ops-section
  ignored_components:
  - graphql_query
- name: observability
  product_stage_group: observability
  slack_alerts_channel: g_observability_alerts
  send_error_budget_weekly_to_slack: true
  slack_error_budget_channel: ops-section
  alerts:
  - gstg
  ignored_components:
  - graphql_query
- name: optimize
  product_stage_group: optimize
  ignored_components:
  - graphql_query
- name: pipeline_authoring
  product_stage_group: pipeline_authoring
  send_slo_alerts_to_team_slack_channel: true
  send_error_budget_weekly_to_slack: true
  slack_channel: g_pipeline-authoring_alerts
  slack_error_budget_channel: ops-section
  ignored_components:
  - graphql_query
- name: pipeline_execution
  product_stage_group: pipeline_execution
  slack_channel: g_pipeline-execution
  slack_alerts_channel: g_pipeline-execution_alerts
  slack_error_budget_channel: ops-section
  send_slo_alerts_to_team_slack_channel: true
  send_error_budget_weekly_to_slack: true
  ignored_components:
  - graphql_query
- name: product_intelligence
  product_stage_group: product_intelligence
  ignored_components:
  - graphql_query
- name: product_planning
  product_stage_group: product_planning
  ignored_components:
  - graphql_query
- name: project_management
  product_stage_group: project_management
  ignored_components:
  - graphql_query
- name: purchase
  product_stage_group: purchase
  ignored_components:
  - graphql_query
- name: pods
  product_stage_group: pods
  ignored_components:
  - graphql_query
- name: source_code
  product_stage_group: source_code
  ignored_components:
  - graphql_query
- name: static_analysis
  product_stage_group: static_analysis
- name: pipeline_insights
  product_stage_group: pipeline_insights
  slack_channel: g_pipeline-insights
  slack_alerts_channel: g_pipeline-insights
  slack_error_budget_channel:
  - ops-section
  - g_pipeline-insights
  send_error_budget_weekly_to_slack: true
  ignored_components:
  - graphql_query
- name: threat_insights
  product_stage_group: threat_insights
  ignored_components:
  - graphql_query
  slack_error_budget_channel: g_govern_threat_insights
  send_error_budget_weekly_to_slack: true
- name: billing_and_subscription_management
  product_stage_group: billing_and_subscription_management
  ignored_components:
  - graphql_query
- name: utilization
  product_stage_group: utilization
  ignored_components:
  - graphql_query
- name: vulnerability_research
  product_stage_group: vulnerability_research
  ignored_components:
  - graphql_query
- name: organization
  product_stage_group: organization
  ignored_components:
  - graphql_query
- name: fulfillment_platform
  product_stage_group: fulfillment_platform
  slack_alerts_channel: s_fulfillment_status
  send_slo_alerts_to_team_slack_channel: true
  ignored_components:
  - graphql_query
